# 批量执行项目
## 目的
在项目中因为需要实现单独的批量程序，用于夜间跑批，所以实现了这个项目。
## 设计思想
### Pojo类
一个任务(Task)里面含有多个执行单元(Unit)和多个执行任务(Schedule)，任务执行时按照执行任务顺序依次执行单元。任务在文件task_demo.xml中
案例中[task_demo.xml](https://git.oschina.net/hcxxiaomo/Data_DB/blob/master/src/main/resources/task_demo.xml "task_demo.xml")文件内容如下：
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 任务配置文件 -->
<task>
	<!-- 执行单元 -->
	<units>
		<unit  name="unitTest1" describe="测试类1" type="single" executeClass="com.xiaomo.unit.UnitTestOne">
			<properties>
				<property name="commitNum" value="2000" />
			</properties>
		</unit>
		<unit  name="unitTest2" describe="测试类2"  type="single" executeClass="com.xiaomo.unit.UnitTestTwo">
			<properties>
				<property name="commitNum" value="1000" />
			</properties>
		</unit>
		<unit  name="unitTest3" describe="测试类3" type="single"  executeClass="com.xiaomo.unit.UnitTestThree">
			<properties>
				<property name="commitNum" value="30" />
			</properties>
		</unit>
		<unit  name="unitTest4" describe="测试类4"  type="single" executeClass="com.xiaomo.unit.UnitTestFour">
			<properties>
				<property name="commitNum" value="40" />
			</properties>
		</unit>
		
		<unit  name="unitTest5" describe="测试类5"  type="single" executeClass="com.xiaomo.unit.UnitTestFive">
			<properties>
				<property name="commitNum" value="40" />
			</properties>
		</unit>
		
		<unit name="multiUnitTest1"  type="multi" describe="多线程测试类1">
		<subunits>
				<subunit ref="unitTest3" />
				<subunit ref="unitTest4" />
			</subunits>
		</unit>
	</units>
	
	<!-- 执行任务 -->
	<schedules>
		<schedule thisUnit="unitTest1" />
		<schedule thisUnit="unitTest2" />
		<schedule thisUnit="multiUnitTest1"  />
		<schedule thisUnit="unitTest5"  />
	</schedules>
</task>
```

[项目的Pojo类](https://git.oschina.net/hcxxiaomo/Data_DB/tree/master/src/main/java/com/xiaomo/model "项目的Pojo类")内容如下：

![Pojo关联UML图](http://xiaomospace.qiniudn.com/git_data2dbdata2db_pojo.svg "Pojo关联UML图")

### 逻辑类
逻辑类的类时序图如图所示

![类时序图](http://xiaomospace.qiniudn.com/git_data2db_execute.svg "类时序图")

1. [TaskBuilder.java](https://git.oschina.net/hcxxiaomo/Data_DB/blob/master/src/main/java/com/xiaomo/core/TaskBuilder.java "TaskBuilder.java")任务构建类

解析task_demo.xml配置文件，将Unit和Schedule构建成Task类。
```java
TaskBuilder.getTaskFromXML(String)
TaskBuilder.getTaskFromXML(File)
TaskBuilder.getTaskFromXML(InputStream)
TaskBuilder.getTaskFromXML(Document)
TaskBuilder.getTaskFromDBString(String)
```
多个重载方法表示从不同的输入源中读取文件内容

```java
TaskBuilder.getUnits(Element)
TaskBuilder.getSingleUnit(Element)
TaskBuilder.getMultiUnit(Element)
TaskBuilder.getSchedules(Element)
TaskBuilder.phraseMultiUnit2SingleUnit()
```
从Element中解析出Units和Schedules的实现

2.[TaskExecute.java](https://git.oschina.net/hcxxiaomo/Data_DB/blob/master/src/main/java/com/xiaomo/core/TaskExecute.java "TaskExecute.java")任务执行类
将Task中的Units按照Schedules的顺序来执行。本来准备应用组合设计模式来实现，但是鉴于树的高度只有两层，所以没有使用遍历，而是直接判断。
```java
TaskExecute.start(Task)
```
执行Task中的Units，如果对应的Unit为SingleUnit类，则单线程执行；如果为MultiUnit类，则多线程执行MultiUnit中的SingleUnits。
```java
TaskExecute.runUnit(Unit)
```
对不同类型的Unit类有相应的方法
- 对单线程执行runSingleUnit(unit);
- 对多线程执行runMultiUnit(unit);

### 具体执行类
[具体执行类](https://git.oschina.net/hcxxiaomo/Data_DB/tree/master/src/main/java/com/xiaomo/unit/ "具体执行类")的类关系如下图所示：

![具体执行类类关系](http://xiaomospace.qiniudn.com/git_data2dbExecuteClass.svg "具体执行类类关系")

所有的执行类只需要继承[ExecuteClass](https://git.oschina.net/hcxxiaomo/Data_DB/blob/master/src/main/java/com/xiaomo/core/ExecuteClass.java "ExecuteClass")并实现execute()方法即可。由于ExecuteClass已经继承了Runnable接口并实现的run()方法，所以可以在多线程中进行调用。PS：这就是模板方法设计模式。
配置文件[task_demo.xml](https://git.oschina.net/hcxxiaomo/Data_DB/blob/master/src/main/resources/task_demo.xml "task_demo.xml")中`<unit/>`配置的属性`<properties/>`会以Map形式注入到`<unit/>`对应的`executeClass`中去，在实现类中可以直接使用。
## 使用方式
1. 按照需要修改[task_demo.xml](https://git.oschina.net/hcxxiaomo/Data_DB/blob/master/src/main/resources/task_demo.xml "task_demo.xml")配置文件中内容；
2. 按照配置文件中配置，实现executeClass类(需要继承[ExecuteClass](https://git.oschina.net/hcxxiaomo/Data_DB/blob/master/src/main/java/com/xiaomo/core/ExecuteClass.java "ExecuteClass")并实现execute()方法），如[具体执行类](https://git.oschina.net/hcxxiaomo/Data_DB/tree/master/src/main/java/com/xiaomo/unit/ "具体执行类")所示；
3. 运行[开始Start](https://git.oschina.net/hcxxiaomo/Data_DB/blob/master/src/main/java/com/xiaomo/app/Start.java "开始Start")即可运行项目。

## 未来工作
1. 在该项目基础上实现实际项目是需要的将数据库导出Data文件和将Data文件导入数据库的应用，便于使用。
2. 将使用注解方式来代替xml配置的方式，便于在实际情况中更好的使用。