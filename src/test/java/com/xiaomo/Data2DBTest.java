package com.xiaomo;

import org.junit.Test;

import com.xiaomo.core.TaskBuilder;
import com.xiaomo.core.TaskExecute;
import com.xiaomo.utils.LogUtil;

public class Data2DBTest {
	
	@Test
	public void testLog() {
		LogUtil log = new LogUtil(this.getClass());
		
		log.info("what is this info");
		log.error("what is this error");
		log.debug("what is this debug");

	}
	
	@Test
	public void testUnitFromXML(){
		System.out.println(TaskBuilder.getTaskFromXML(TaskBuilder.class.getClassLoader().getResourceAsStream("task_demo.xml")));
	}
	
	@Test
	public void testTaskRun(){
		TaskExecute.start(TaskBuilder.getTaskFromXML(TaskBuilder.class.getClassLoader().getResourceAsStream("task_demo.xml")));
	}

}
