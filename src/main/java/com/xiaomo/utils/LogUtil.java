package com.xiaomo.utils;

import java.util.Date;

public class LogUtil  {
	private Class<?> c;

	public LogUtil(Class<?> c) {
		super();
		this.c = c;
	}

	public LogUtil getLogger(Class<?> c){
		return new LogUtil(c);
	}
	
	public void info(Object s){
		System.out.println(String.format("%s [INFO] %s --> %s ",(new Date()), c.getName(),s));
	}
	
	public void debug(Object s){
		System.out.println(String.format("%s [DEBUG] %s --> %s ",(new Date()).toString(), c.getName(),s));
	}
	
	public void error(Object s){
		System.err.println(String.format("%s [ERROR] %s --> %s ",(new Date()).toString(), c.getClass().getName(),s.toString()));
	}
	

}

