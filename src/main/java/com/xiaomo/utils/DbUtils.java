package com.xiaomo.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DbUtils {
	private static Properties prop;
	private static  String CONFIG = "jdbc.properties";
	private static List<Connection> conns;
	private static Connection conn;
	
	static{
		try{
			conns = new ArrayList<Connection>();
			prop = new Properties();
			prop.load(DbUtils.class.getClassLoader().getResourceAsStream(CONFIG));
			
			Class.forName(prop.getProperty("sDriverName"));
			
			for (int i = 0; i < 10; i++) {
				conn = DriverManager.getConnection(prop.getProperty("sUrl"), prop.getProperty("sUserName"), prop.getProperty("sUserPass"));
				conns.add(conn);
			}
			}catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	//get conn from List
	public static  Connection getConnection(){
		return conns.remove(0);
	}
	
	//close conn return to conns
	public static  void  close(Connection conn){
		if (conn != null) {
			conns.add(conn);
		}
	}
	public static void main(String[] args) {
		try {
			
			Connection con =  DbUtils.getConnection();
			String sql = "SELECT * FROM test_user limit ?,?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, 4);
			ps.setInt(2, 8);
			ResultSet rs = ps.executeQuery();
			int columeCount = (rs.getMetaData().getColumnCount());
			while (rs.next()) {
				for (int i = 0; i <columeCount; i++) {
					System.out.print(rs.getString(i+1)+"\t");
				}
				System.out.println();
			}
			DbUtils.close(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
