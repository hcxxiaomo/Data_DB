package com.xiaomo.core;

import java.util.Map;

public abstract class ExecuteClass implements Runnable {
	
	protected Map<String,Object> properties;
	
	public Map<String, Object> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	public abstract void  execute();

	@Override
	public void run() {
		this.execute();
	}
	
	
}
