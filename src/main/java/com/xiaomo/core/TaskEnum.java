package com.xiaomo.core;

enum TaskEnum {
	task,units,unit,properties,property,
	subunits,subunit,
	schedules,schedule,
	name,describe,type,value,
	single,multi
}
