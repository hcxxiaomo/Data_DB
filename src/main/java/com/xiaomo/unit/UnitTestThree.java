package com.xiaomo.unit;

import com.xiaomo.core.ExecuteClass;
import com.xiaomo.model.SingleUnit;

public class UnitTestThree extends ExecuteClass {

	@Override
	public void execute() {
		try {
			System.out.println("进入UnitTestThree等待时间3s");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.print(this.getClass().getName().concat(" UnitTestThree  execute() method-->"));
		if (properties != null) {
			System.out.println(properties.toString());
		}else {
			System.out.println();
		}
	}
}
