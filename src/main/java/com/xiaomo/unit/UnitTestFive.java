package com.xiaomo.unit;

import com.xiaomo.core.ExecuteClass;

public class UnitTestFive extends ExecuteClass {

	@Override
	public void execute() {
		System.out.print(this.getClass().getName().concat(" UnitTestFive  execute() method-->"));
		if (properties != null) {
			System.out.println(properties.toString());
		}else {
			System.out.println();
		}
	}

}
