package com.xiaomo.unit;

import com.xiaomo.core.ExecuteClass;
import com.xiaomo.model.SingleUnit;

public class UnitTestTwo extends ExecuteClass {

	@Override
	public void execute() {
		System.out.print(this.getClass().getName().concat(" UnitTestTwo execute() method-->"));
		if (properties != null) {
			System.out.println(properties.toString());
		}else {
			System.out.println();
		}
	}
}
