package com.xiaomo.model;

import java.util.Map;

import com.xiaomo.core.ExecuteClass;
/**
 * 执行单元Pojo
 * @author xiaomo
 */
public class SingleUnit extends Unit {
	private ExecuteClass executeClass;
	private String executeClassName;
	private Map<String,Object> properties;
	
	public ExecuteClass getExecuteClass() {
		return executeClass;
	}
	public void setExecuteClass(ExecuteClass executeClass) {
		this.executeClass = executeClass;
	}
	public Map<String, Object> getProperties() {
		return properties;
	}
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
	public String getExecuteClassName() {
		return executeClassName;
	}
	public void setExecuteClassName(String executeClassName) {
		this.executeClassName = executeClassName;
	}
	
	@Override
	public String toString() {
		return new StringBuilder().append("Unit [name=").append(name).append(", describe=").append( describe
				).append(", executeClass=").append( executeClass ).append( ", executeClassName="
				).append( executeClassName ).append( ", type=").append( type ).append( ", properties="
				).append( properties ).append( "]").toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((describe == null) ? 0 : describe.hashCode());
		result = prime * result
				+ ((executeClass == null) ? 0 : executeClass.hashCode());
		result = prime
				* result
				+ ((executeClassName == null) ? 0 : executeClassName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((properties == null) ? 0 : properties.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SingleUnit other = (SingleUnit) obj;
		if (describe == null) {
			if (other.describe != null)
				return false;
		} else if (!describe.equals(other.describe))
			return false;
		if (executeClass == null) {
			if (other.executeClass != null)
				return false;
		} else if (!executeClass.equals(other.executeClass))
			return false;
		if (executeClassName == null) {
			if (other.executeClassName != null)
				return false;
		} else if (!executeClassName.equals(other.executeClassName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	

	
	
}
