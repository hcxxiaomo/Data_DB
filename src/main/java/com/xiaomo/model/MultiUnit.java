package com.xiaomo.model;

import java.util.List;

public class MultiUnit extends Unit {

	private List<SingleUnit> subunits;

	public List<SingleUnit> getSubunits() {
		return subunits;
	}

	public void setSubunits(List<SingleUnit> subunits) {
		this.subunits = subunits;
	}

	@Override
	public String toString() {
		return "MultiUnit [subunits=" + subunits + ", name=" + name
				+ ", describe=" + describe + ", type=" + type + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((subunits == null) ? 0 : subunits.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MultiUnit other = (MultiUnit) obj;
		if (subunits == null) {
			if (other.subunits != null)
				return false;
		} else if (!subunits.equals(other.subunits))
			return false;
		return true;
	}
	
	
	
}
