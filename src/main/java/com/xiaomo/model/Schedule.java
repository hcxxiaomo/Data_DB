package com.xiaomo.model;

public class Schedule {
	
	private Unit thisUnit;
	
	public Unit getThisUnit() {
		return thisUnit;
	}
	public void setThisUnit(Unit thisUnit) {
		this.thisUnit = thisUnit;
	}
	@Override
	public String toString() {
		return "Schedule [thisUnit=" + thisUnit + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((thisUnit == null) ? 0 : thisUnit.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Schedule other = (Schedule) obj;
		if (thisUnit == null) {
			if (other.thisUnit != null)
				return false;
		} else if (!thisUnit.equals(other.thisUnit))
			return false;
		return true;
	}
	

	

}
