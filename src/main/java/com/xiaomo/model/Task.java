package com.xiaomo.model;

import java.util.List;
import java.util.Map;

public class Task {
	private Map<String,Unit> units;
	private List<Schedule> schedules;
	
	public Map<String, Unit> getUnits() {
		return units;
	}
	public void setUnits(Map<String, Unit> units) {
		this.units = units;
	}
	public List<Schedule> getSchedules() {
		return schedules;
	}
	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}
	@Override
	public String toString() {
		return "Task [units=" + units + ", schedules=" + schedules + "]";
	}
	

}
