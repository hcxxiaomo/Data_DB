package com.xiaomo.app;

import com.xiaomo.core.TaskBuilder;
import com.xiaomo.core.TaskExecute;

public class Start {
	
	public static void main(String[] args) {
		TaskExecute.start(TaskBuilder.getTaskFromXML(TaskBuilder.class.getClassLoader().getResourceAsStream("task_demo.xml")));

	}

}
